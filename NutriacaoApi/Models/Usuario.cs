namespace UsuarioBackend.Models
{
    public abstract class Usuario
    {
        public string? Id { get; set; }
        public string Nome { get; set; } = "";
        public string Telefone { get; set; } = "";
        public string CPF { get; set; } = "";
    }
}
