using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NutriacaoApi.Models;
using PacienteBackend.Models;
using NutricionistaBackend.Models;

namespace NutriacaoApi.Models
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Paciente> Pacientes { get; set; } = null!;
        public DbSet<Nutricionista> Nutricionistas { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql($"Server=localhost;Database=Nutriacao;User=root;Password=root;Port=3306;",
            new MySqlServerVersion(new Version(8, 0, 34)));
        }
    }
}
