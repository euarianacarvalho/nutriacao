namespace PacienteBackend.Models
{
    using UsuarioBackend.Models;

    public class Paciente : Usuario
    {
        public int Altura {get; set;}
        public double Peso {get; set;}
    }
}
