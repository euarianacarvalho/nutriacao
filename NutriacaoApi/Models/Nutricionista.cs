namespace NutricionistaBackend.Models
{
    using UsuarioBackend.Models;

    public class Nutricionista : Usuario
    {
        public string CRN { get; set; } = "";
    }
}
