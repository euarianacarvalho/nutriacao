using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PacienteBackend.Models;
using UsuarioBackend.Models;
using NutriacaoApi.Models;

namespace PacienteBackend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PacienteController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public PacienteController(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Paciente>> Get()
        {
            if (_dbContext.Pacientes == null)
                return NotFound("Nenhum paciente cadastrado.");

            return _dbContext.Pacientes;
        }

        [HttpGet("{id}")]
        public ActionResult<Paciente> GetId(string id)
        {
            var obj = _dbContext.Pacientes.FirstOrDefault(x => x.Id == id);

            if (obj == null)
                return NotFound("Não foi encontrado nenhum paciente com o identificador informado.");

            return obj;
        }

        [HttpPost]
        public ActionResult<Paciente> Post(Paciente obj)
        {
            if (string.IsNullOrWhiteSpace(obj.Id))
                obj.Id = Guid.NewGuid().ToString();

            _dbContext.Pacientes.Add(obj);
            _dbContext.SaveChanges();

            return CreatedAtAction(
                nameof(GetId),
                new { id = obj.Id },
                obj
            );
        }

        [HttpPut("{id}")]
        public IActionResult Put(string id, Paciente obj)
        {
            if (id != obj.Id)
                return BadRequest("O identificador informado difere do identificador do objeto");

            _dbContext.Pacientes.Update(obj);
            _dbContext.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (_dbContext.Pacientes == null)
                return NotFound("Nenhum paciente cadastrado.");

            var obj = _dbContext.Pacientes.FirstOrDefault(x => x.Id == id);

            if (obj == null)
                return NotFound("Não foi encontrado nenhum paciente com o identificador informado.");

            _dbContext.Pacientes.Remove(obj);
            _dbContext.SaveChanges();

            return NoContent();
        }
    }
}
