using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NutricionistaBackend.Models;
using UsuarioBackend.Models;
using NutriacaoApi.Models;

namespace NutricionistaBackend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NutricionistaController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public NutricionistaController(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Nutricionista>> Get()
        {
            if (_dbContext.Nutricionistas == null)
                return NotFound("Nenhum nutricionista cadastrado.");

            return _dbContext.Nutricionistas;
        }

        [HttpGet("{id}")]
        public ActionResult<Nutricionista> GetId(string id)
        {
            var obj = _dbContext.Nutricionistas.FirstOrDefault(x => x.Id == id);

            if (obj == null)
                return NotFound("Não foi encontrado nenhum nutricionista com o identificador informado.");

            return obj;
        }

        [HttpPost]
        public ActionResult<Nutricionista> Post(Nutricionista obj)
        {
            if (string.IsNullOrWhiteSpace(obj.Id))
                obj.Id = Guid.NewGuid().ToString();

            _dbContext.Nutricionistas.Add(obj);
            _dbContext.SaveChanges();

            return CreatedAtAction(
                nameof(GetId),
                new { id = obj.Id },
                obj
            );
        }

        [HttpPut("{id}")]
        public IActionResult Put(string id, Nutricionista obj)
        {
            if (id != obj.Id)
                return BadRequest("O identificador informado difere do identificador do objeto");

            _dbContext.Nutricionistas.Update(obj);
            _dbContext.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (_dbContext.Nutricionistas == null)
                return NotFound("Nenhum nutricionista cadastrado.");

            var obj = _dbContext.Nutricionistas.FirstOrDefault(x => x.Id == id);

            if (obj == null)
                return NotFound("Não foi encontrado nenhum nutricionista com o identificador informado.");

            _dbContext.Nutricionistas.Remove(obj);
            _dbContext.SaveChanges();

            return NoContent();
        }
    }
}
