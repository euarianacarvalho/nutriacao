using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NutriacaoApi.Models;
using PacienteBackend.Models;
using NutricionistaBackend.Models;
using UsuarioBackend.Models;

namespace UserBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IAuthService authService;

        public UsuarioController(UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            RoleManager<IdentityRole> roleManager, IAuthService authService)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.authService = authService;
            this.roleManager = roleManager;
        }

        [HttpPost("Criar")]
        public async Task<ActionResult<string>> CreateUser([FromBody] UserInfo model)
        {
            return await CreateUserExecute(model);
        }

        [HttpPost("CriarAdmin")]
        public async Task<ActionResult<string>> CreateAdminUser([FromBody] UserInfo model)
        {
            return await CreateUserExecute(model, "Admin");
        }

        private async Task<ActionResult<string>> CreateUserExecute(UserInfo userInfo, string roleName = "Member")
        {
            var ret = await authService.Register(userInfo, roleName);

            if (ret.Status == EReturnStatus.Success)
            {
                var retToken = await authService.Login(userInfo);

                if (retToken.Status == EReturnStatus.Success)
                    return Ok(retToken.Result);
                else
                    return BadRequest(retToken.Result);
            }
            else
                return BadRequest(ret.Result);
        }

        [HttpPost("Login")]
        public async Task<ActionResult<string>> Login([FromBody] UserInfo userInfo)
        {
            var retToken = await authService.Login(userInfo);

            if (retToken.Status == EReturnStatus.Success)
                return Ok(retToken.Result);
            else
                return BadRequest(retToken.Result);
        }
    }
}
