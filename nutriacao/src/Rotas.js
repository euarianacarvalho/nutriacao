import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Paciente from "./components/Paciente";
import Nutricionista from "./components/Nutricionista";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Login from './components/Login';
import Registrar from "./components/Registrar";

const Rotas = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Layout />}>
          <Route index element={<Home />} />
          <Route path='login' element={<Login />}></Route>
          <Route path='registrar' element={<Registrar />}>
            <Route path=':acao' element={<Registrar />} />
          </Route>
          <Route path='paciente' element={<Paciente />}>
            <Route path=':acao' element={<Paciente />} />
            <Route path=':acao/:id' element={<Paciente />} />
          </Route>
          <Route path='nutricionista' element={<Nutricionista />}>
            <Route path=':acao' element={<Nutricionista />} />
            <Route path=':acao/:id' element={<Nutricionista />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Rotas;
