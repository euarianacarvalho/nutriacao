import React from "react";
import { useParams } from "react-router-dom";
import Crud from "../Crud";

const Paciente = () => {
    const { id, acao } = useParams();

    const configCampos = {
        titulos: ["Nome", "Altura", "Peso"],
        propriedades: ["nome", "altura", "peso"],
    };

    const novoObjeto = () => {
        return { id: "", nome: "", altura: 0, peso: 0 };
    };

    const campos = (somenteLeitura, obj, alterarCampo) => {
        return (
            <>
                <div className="form-group">
                    <label htmlFor="nome">Nome</label>
                    <input
                        type="text"
                        readOnly={somenteLeitura}
                        value={obj.nome}
                        onChange={(e) => alterarCampo("nome", e.target.value)}
                        className="form-control"
                        id="nome"
                        name="nome"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="altura">Altura</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.altura}
                        onChange={(e) => alterarCampo("altura", e.target.value)}
                        className="form-control"
                        id="altura"
                        name="altura"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="peso">Peso</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.peso}
                        onChange={(e) => alterarCampo("peso", e.target.value)}
                        className="form-control"
                        id="peso"
                        name="peso"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="telefone">Telefone</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.telefone}
                        onChange={(e) => alterarCampo("telefone", e.target.value)}
                        className="form-control"
                        id="telefone"
                        name="telefone"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="cpf">CPF</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.cpf}
                        onChange={(e) => alterarCampo("cpf", e.target.value)}
                        className="form-control"
                        id="cpf"
                        name="cpf"
                    />
                </div>
            </>
        );
    };

    return (
        <Crud
            entidade="paciente"
            entidadeNomeAmigavel="Paciente"
            entidadeNomeAmigavelPlural="Pacientes"
            id={id}
            acao={acao}
            configCampos={configCampos}
            campos={campos}
            novoObjeto={novoObjeto}
            // outras props ou métodos, se necessário
        />
    );
};

export default Paciente;
