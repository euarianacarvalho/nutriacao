import React from "react";
import { useParams } from "react-router-dom";
import Crud from "../Crud";

const Nutricionista = () => {
    const { id, acao } = useParams();

    const configCampos = {
        titulos: ["Nome", "CRN"],
        propriedades: ["nome", "crn"],
    };

    const novoObjeto = () => {
        return { id: "", nome: "", crn: "" };
    };

    const campos = (somenteLeitura, obj, alterarCampo) => {
        return (
            <>
                <div className="form-group">
                    <label htmlFor="nome">Nome</label>
                    <input
                        type="text"
                        readOnly={somenteLeitura}
                        value={obj.nome}
                        onChange={(e) => alterarCampo("nome", e.target.value)}
                        className="form-control"
                        id="nome"
                        name="nome"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="crn">CRN</label>
                    <input
                        type="text"
                        readOnly={somenteLeitura}
                        value={obj.crn}
                        onChange={(e) => alterarCampo("crn", e.target.value)}
                        className="form-control"
                        id="crn"
                        name="crn"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="telefone">Telefone</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.telefone}
                        onChange={(e) => alterarCampo("telefone", e.target.value)}
                        className="form-control"
                        id="telefone"
                        name="telefone"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="cpf">CPF</label>
                    <input
                        type="number"
                        readOnly={somenteLeitura}
                        value={obj.cpf}
                        onChange={(e) => alterarCampo("cpf", e.target.value)}
                        className="form-control"
                        id="cpf"
                        name="cpf"
                    />
                </div>
            </>
        );
    };

    return (
        <Crud
            entidade="nutricionista"
            entidadeNomeAmigavel="Nutricionista"
            entidadeNomeAmigavelPlural="Nutricionistas"
            id={id}
            acao={acao}
            configCampos={configCampos}
            campos={campos}
            novoObjeto={novoObjeto}
        />
    );
};

export default Nutricionista;
