import React from "react";
import { Outlet, Link, useNavigate } from "react-router-dom";
import { estaAutenticado, logout, nomeUsuarioLogado, usuarioPossuiPermissao } from "../auth";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';

const Layout = () => {
  const navigate = useNavigate();

  const sair = () => {
    logout();
    navigate('/');
  };

  const renderLoginLogout = () => {
    if (estaAutenticado()) {
      return (
        <li className="nav-item">
          <button className="nav-link btn" onClick={sair}>Logout</button>
        </li>
      );
    } else {
      return (
        <li className="nav-item">
          <Link to="/login" className="nav-link">Login</Link>
        </li>
      );
    }
  };

  const nomeUsuario = nomeUsuarioLogado();
  const usuarioIdentificacao = nomeUsuario ? <p className="mt-2">Bem-vindo, {nomeUsuario}!</p> : null;

  const cadastrarAdmin = usuarioPossuiPermissao('Admin') ? (
    <Link to="/registrar/admin" className="dropdown-item">Criar usuário admin</Link>
  ) : null;

  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand">NutriAção</Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">Home</Link>
            </li>
            <li className="nav-item dropdown">
              <Link to="#" className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cadastros
              </Link>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link to="/paciente" className="dropdown-item">Paciente</Link>
                <Link to="/nutricionista" className="dropdown-item">Nutricionista</Link>
                {cadastrarAdmin}
              </div>
            </li>
            {usuarioIdentificacao}
            {renderLoginLogout()}
          </ul>
        </div>
      </nav>
      <div className="container mt-4">
        <Outlet />
      </div>
    </React.Fragment>
  );
};

export default Layout;
