import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const BlogPost = ({ title, content, author, date }) => {
  return (
    <div className="card mb-3">
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{content}</p>
        <p className="card-text">
          <small className="text-muted">
            Autor: {author} | Data: {date}
          </small>
        </p>
      </div>
    </div>
  );
};

const Home = () => {
  return (
    <div className="container mt-4">
      <h1>Alimentação Saudável e Nutrição</h1>
      <BlogPost
        title="Benefícios de uma Dieta Equilibrada"
        content="Descubra os benefícios de manter uma dieta equilibrada para melhorar sua saúde geral."
        author="Nutricionista Jane Doe"
        date="2023-11-22"
      />
      <BlogPost
        title="Receitas Saudáveis para o Café da Manhã"
        content="Experimente essas deliciosas e saudáveis receitas para começar o dia com energia."
        author="Chef John Smith"
        date="2023-11-23"
      />
      {/* Adicione mais posts conforme necessário */}
    </div>
  );
};

export default Home;
